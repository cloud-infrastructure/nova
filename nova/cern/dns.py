#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import dns.resolver
import socket

import nova.conf

CONF = nova.conf.CONF

LIBRARIES = {
    'socket': socket.gethostbyname,
    'dnspython': dns.resolver.resolve_name,
}


class Dns(object):
    def __init__(self):
        self.library = LIBRARIES[CONF.cern.dns_library]
        self.domain = CONF.cern.dns_domain

    def gethostbyname(self, hostname):
        try:
            self.library(f"{hostname}{self.domain}")
            return hostname
        except Exception:
            return False
