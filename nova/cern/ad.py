#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from suds.client import Client

import logging as pylog
import nova.conf
from nova import exception
from oslo_log import log as logging


CONF = nova.conf.CONF
LOG = logging.getLogger(__name__)
pylog.getLogger('suds.client').setLevel(pylog.CRITICAL)


class ActiveDirectory(object):
    def __init__(self):
        url = CONF.cern.activedirectory_endpoint
        self.client = Client(url, cache=None)

    def register(self, hostname):
        try:
            result = self.client.service.CheckComputer(hostname)
            if result is not None:
                LOG.warning("AD update failed - %s - %s",
                    hostname, str(result))
        except Exception:
            raise exception.CernActiveDirectory()

    def delete(self, hostname):
        try:
            result = self.client.service.DeleteComputer(hostname)
            if result is not None:
                LOG.warning("AD delete failed - %s - %s",
                    hostname, str(result))
        except Exception as e:
            LOG.warning("Cannot delete VM from AD. %s", e)
