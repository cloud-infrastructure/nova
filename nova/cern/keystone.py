#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from keystoneclient.v3 import client as keyclient
from nova.cern import exception
import nova.conf
from oslo_log import log as logging


CONF = nova.conf.CONF
LOG = logging.getLogger(__name__)


class Keystone(object):
    def __init__(self, context, endpoint=None):
        auth_token = context.auth_token
        if not endpoint:
            endpoint = CONF.cern.keystone_endpoint
        self.client = keyclient.Client(token=auth_token, endpoint=endpoint)

    def _get_project(self, project_id):
        try:
            project = self.client.projects.get(project_id)
        except Exception as e:
            LOG.error("Cannot get project: %s", e)
            raise exception.CernLanDB()
        return project

    def get_project_mainuser(self, project_id):
        project = self._get_project(project_id)
        mainuser = None
        if project and 'landb-mainuser' in project.__dict__.keys():
            mainuser = project.__dict__['landb-mainuser']
        return mainuser

    def get_project_responsible(self, project_id):
        project = self._get_project(project_id)
        responsible = None
        if project and 'landb-responsible' in project.__dict__.keys():
            responsible = project.__dict__['landb-responsible']
        return responsible

    def get_cells_mapping(self, project_id):
        cells_mapping = []
        project = self._get_project(project_id)
        if project and 'cells_mapping' in project.__dict__.keys():
            temp = project.__dict__['cells_mapping']
            for x in temp.split(','):
                cells_mapping.append(x.strip())
        return cells_mapping
