#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import functools

from oslo_log import log as logging
from oslo_utils import timeutils

from nova.cern.keystone import Keystone
from nova import context
from nova import exception
from nova import objects

LOG = logging.getLogger(__name__)


def default_filter(ctxt, request_spec):
    def _retrieve_metadata_property(aggregates, key):
        for aggregate in aggregates:
            for k, value in aggregate.metadata.items():
                if k == key:
                    return value
        return None

    def _filter_by_metadata_property(aggregates, key, value):
        filtered = []
        for aggregate in aggregates:
            for k, v in aggregate.metadata.items():
                if k == key and v == value:
                    filtered.append(aggregate)
        return filtered

    if ('requested_destination' in request_spec and
            request_spec.requested_destination and
            'cell' in request_spec.requested_destination):
        # If there is a requested_destination it means that
        # it's a live migration with host specified or within cell
        destination = request_spec.requested_destination

        # Try to get the source host if exists in any of the DBs
        im = objects.InstanceMapping.get_by_instance_uuid(
            ctxt, request_spec.instance_uuid)
        with context.target_cell(ctxt, im.cell_mapping) as cctxt:
            try:
                host = objects.Instance.get_by_uuid(
                    cctxt, request_spec.instance_uuid).host
            except exception.InstanceNotFound:
                host = None

        if host:
            # If there is a host (source or target), we should restrict
            # to the aggregates with the same netcluster property (if present)
            aggregates_migrate = objects.AggregateList.get_by_host(
                ctxt, host)
            # We can increase this list if there is a netcluster property
            # In that case we can get all aggregates that have that property
            # defined
            netcluster = _retrieve_metadata_property(
                aggregates_migrate, 'netcluster')
            mode = _retrieve_metadata_property(
                aggregates_migrate, 'mode')

            if netcluster:
                # Retrieve all aggregates and reduce them to just the ones that
                # have the netcluster property that matches
                aggregates_migrate = _filter_by_metadata_property(
                    objects.AggregateList.get_all(ctxt),
                    'netcluster', netcluster)

            if mode and (
                    not destination.obj_attr_is_set('host') or
                    destination.host):
                # In case we don't have a target host we need to ensure
                # the mode of the aggregate as well
                aggregates_migrate = _filter_by_metadata_property(
                    aggregates_migrate, 'mode', mode)
        else:
            # Failback scenario, get all the aggregates that are inside
            # the cell
            aggregates_migrate = objects.AggregateList.get_by_metadata(
                ctxt, key='cell_name', value=destination.cell.name)
        destination.require_aggregates(
            [aggr.uuid for aggr in aggregates_migrate])
        return

    # Default filter entry point for other scenarios
    # Project cell mapping
    client_key = Keystone(ctxt)
    project_cells_mapping = client_key.get_cells_mapping(
        request_spec.project_id)

    aggregates_uuid = set([])
    if project_cells_mapping:
        # If the project has a cell mapping we need to retrieve
        # all aggregates that cell_name is in cells_mapping property
        # of the project
        LOG.debug("project_cells_mapping: %s", project_cells_mapping)

        # Get all aggregates
        aggregates_cell = objects.AggregateList.get_by_metadata(
            ctxt, key='cell_name')
        for agg in aggregates_cell:
            for key, value in agg.metadata.items():
                if (key.startswith('cell_name') and
                        (value in project_cells_mapping)):
                    aggregates_uuid.add(agg.uuid)
                    break
    else:
        # If the project is not mapped we can just add all the aggregates
        # which cell_type property is 'default' stored in aggregates_default
        aggregates_default = objects.AggregateList.get_by_metadata(
            ctxt, key='cell_type', value='default')
        for agg in aggregates_default:
            aggregates_uuid.add(agg.uuid)

    LOG.debug("aggregates_uuid %s", aggregates_uuid)

    # Availability zone hints
    avz_hint = request_spec.availability_zone
    if avz_hint:
        aggregates_avz = objects.AggregateList.get_by_metadata(
            ctxt,
            key='availability_zone',
            value=avz_hint)
        aggregates_avz_uuid = [agg.uuid for agg in aggregates_avz]
        LOG.debug("aggregates_avz_uuid: %s", aggregates_avz_uuid)
        aggregates_uuid.intersection_update(aggregates_avz_uuid)

    LOG.debug("aggregates_uuid: %s", aggregates_uuid)

    if not aggregates_uuid:
        LOG.error('No available host aggregate found matching the request '
                  'constraints')
        raise exception.RequestFilterFailed(
            reason=('No available host aggregate found matching the request'
                    'constraints')
        )

    if ('requested_destination' not in request_spec or
        request_spec.requested_destination is None):
        request_spec.requested_destination = objects.Destination()
    destination = request_spec.requested_destination
    destination.require_aggregates(aggregates_uuid)


def flavor_cells_mapping(ctxt, request_spec):
    raw_mapping = request_spec.flavor.extra_specs.get('cells_mapping', '')

    cells_mapping = []
    for x in raw_mapping.split(','):
        cells_mapping.append(x.strip())

    LOG.debug("flavor_cells_mapping flavor_cells_mapping: %s", cells_mapping)

    aggregates_uuid = set([])
    aggregates_cell = objects.AggregateList.get_by_metadata(ctxt,
                                                            key='cell_name')

    for agg in aggregates_cell:
        for key, value in agg.metadata.items():
            if key.startswith('cell_name') and (value in cells_mapping):
                aggregates_uuid.add(agg.uuid)
                break

    LOG.debug("flavor_cells_mapping aggregates_uuid: %s", aggregates_uuid)

    if aggregates_uuid:
        request_spec.requested_destination = objects.Destination()
        destination = request_spec.requested_destination
        destination.require_aggregates(aggregates_uuid)


def flavor_aggregates_mapping(ctxt, request_spec):
    raw_mapping = request_spec.flavor.extra_specs.get('aggregates_mapping', '')

    aggregates_mapping = []
    for x in raw_mapping.split(','):
        aggregates_mapping.append(x.strip())

    LOG.debug("flavor_aggregates_mapping flavor_aggregates_mapping: %s",
              aggregates_mapping)

    aggregates_uuid = set([])
    aggregates = objects.AggregateList.get_by_metadata(
        ctxt, key='flavor_aggregates_mapping')

    for agg in aggregates:
        for key, value in agg.metadata.items():
            if (key.startswith('flavor_aggregates_mapping') and
                    value in aggregates_mapping):
                aggregates_uuid.add(agg.uuid)
                break

    LOG.debug("flavor_aggregates_mapping aggregates_uuid: %s", aggregates_uuid)

    if aggregates_uuid:
        request_spec.requested_destination = objects.Destination()
        destination = request_spec.requested_destination
        destination.require_aggregates(aggregates_uuid)


def trace_request_filter(fn):
    @functools.wraps(fn)
    def wrapper(ctxt, request_spec):
        timer = timeutils.StopWatch()
        ran = False
        with timer:
            try:
                ran = fn(ctxt, request_spec)
            finally:
                if ran:
                    # Only log info if the filter was enabled and not
                    # excluded for some reason
                    LOG.debug('Request filter %r took %.1f seconds',
                        fn.__name__, timer.elapsed())
        return ran
    return wrapper


@trace_request_filter
def enforce_aggregate_mode(
    ctxt: context.RequestContext,
    request_spec: 'objects.RequestSpec'
) -> bool:
    def _retrieve_metadata_property(aggregates, key):
        for aggregate in aggregates:
            for k, value in aggregate.metadata.items():
                if k == key:
                    return value
        return None

    def _filter_by_metadata_property(aggregates, key, value):
        filtered = []
        for aggregate in aggregates:
            for k, v in aggregate.metadata.items():
                if k == key and v == value:
                    filtered.append(aggregate)
        return filtered

    """Keep the mode of the aggregate.

    In case of migrations this filter will keep the mode of the hypervisor
    unless there is a specific target for the migration.
    As we have many aggregates under the same segment with different purposes,
    we would like to avoid unexpected migrations from shared to optimized
    triggered by an evacuation or a HW issue. Only directed migrations through
    host will be allowed to jump these boundaries.
    """
    if ('requested_destination' in request_spec and
            request_spec.requested_destination and
            'cell' in request_spec.requested_destination):
        # If there is a requested_destination it means that
        # it's a live migration with host specified or within cell
        destination = request_spec.requested_destination

        # Try to get the source host if exists in any of the DBs
        im = objects.InstanceMapping.get_by_instance_uuid(
            ctxt, request_spec.instance_uuid)
        with context.target_cell(ctxt, im.cell_mapping) as cctxt:
            try:
                host = objects.Instance.get_by_uuid(
                    cctxt, request_spec.instance_uuid).host
            except exception.InstanceNotFound:
                host = None

        if host and (not destination.obj_attr_is_set('host') or
                destination.host):
            # If there is a host (source or target), and we don't have
            # a target we should restrict to the aggregates with the
            # same mode property (if present)
            aggregates_migrate = objects.AggregateList.get_by_host(
                ctxt, host)

            mode = _retrieve_metadata_property(
                aggregates_migrate, 'mode')

            if mode:
                aggregates_migrate = _filter_by_metadata_property(
                    aggregates_migrate, 'mode', mode)
                destination.require_aggregates(
                    [aggr.uuid for aggr in aggregates_migrate])
    return True


def anti_affinity_filter(instance_group, host_state):
    if 'rules' in instance_group:
        rules = instance_group.rules

        # If there is a metric in the rule we'll try to fetch the
        # metrics from compute nodes and identify the ones that
        # have that metric and key and not share the value more than
        # what has been specified in the max_server value
        scope = rules['scope'] if (rules and 'scope' in rules) else None
        if rules and 'max_server_per_scope' in rules:
            max_server_per_scope = rules['max_server_per_scope']
        else:
            max_server_per_scope = 1

        if scope and scope in objects.LocationMapping.fields:
            # There is a scope defined we can fetch the location,zone from
            host_location = set([get_node_location_scoped(
                host_state.nodename, scope)])
            group_location = get_nodes_location_scoped(
                instance_group.hosts, scope)
            location_on_host = host_location.intersection(group_location)
            return len(location_on_host) < max_server_per_scope
    return None


def affinity_filter(instance_group, host_state):
    if 'rules' in instance_group:
        rules = instance_group.rules
        ctxt = context.RequestContext()

        # If there is a metric in the rule we'll try to fetch the
        # location from compute nodes and identify the ones that
        # have that location
        scope = rules['scope'] if (rules and 'scope' in rules) else None

        if scope and scope in objects.LocationMapping.fields:
            # There is a scope defined we can fetch the location,zone from
            host_location = get_node_location_scoped(
                ctxt, host_state.nodename, scope)
            group_location = get_nodes_location_scoped(
                ctxt, instance_group.hosts, scope)
            return host_location in group_location
    return None


def affinity_weighter(instance_group, host_state):
    if 'rules' in instance_group:
        rules = instance_group.rules
        ctxt = context.RequestContext()
        scope = rules['scope'] if (rules and 'scope' in rules) else None
        if scope and scope in objects.LocationMapping.fields:
            # There is a scope defined we can fetch the location from
            host_location = set([get_node_location_scoped(
                ctxt, host_state.nodename, scope)])
            group_location = get_nodes_location_scoped(
                ctxt, instance_group.hosts, scope)
            location_on_host = host_location.intersection(group_location)
            return len(location_on_host)
    return None


def get_node_location_scoped(ctxt, node, attr):
    lm = objects.LocationMapping.get_by_host(ctxt, node)
    return getattr(lm, attr)


def get_nodes_location_scoped(ctxt, nodes, attr):
    locations = set()
    for node in nodes:
        locations.add([get_node_location_scoped(ctxt, node, attr)])
    return locations


PREREQUEST_FILTERS = [
    enforce_aggregate_mode
]

REQUEST_FILTERS = [
    default_filter,
    flavor_cells_mapping,
    flavor_aggregates_mapping
]
