#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import ipaddress
import random
import string
import time

from functools import reduce
from webob import exc

from nova.cern.ad import ActiveDirectory
from nova.cern.dns import Dns
from nova.cern import exception
from nova.cern.keystone import Keystone
from nova.cern.landb import LanDB
from nova.cern.neutron import Neutron
from nova.cern.xldap import Xldap
from nova.compute import vm_states
import nova.conf

from nova import context as nova_context
from nova import exception as nova_exception
from nova.image import glance
from nova.network import model as network_model
from nova import objects
from nova import utils
from nova.virt.ironic import client_wrapper as ironic_wrapper
from nova.volume import cinder
from oslo_log import log as logging


CONF = nova.conf.CONF
LOG = logging.getLogger(__name__)

VM_LOCATION = {'Floor': '0', 'Room': '0', 'Building': '0'}
VM_MANUFACTURER = 'KVM'
VM_MODEL = 'VIRTUAL MACHINE'
VM_TAG = 'OPENSTACK VM'


def attach_interface(network_api, context, instance, port_id):
    try:
        neutron = Neutron(network_api, context)
        cluster_name = None
        mac_address = None
        subnet_names = []
        ipv4s = []
        ipv6s = []

        port_info = neutron.show_port(port_id)
        mac_address = port_info["port"]["mac_address"]
        mac_address = mac_address.replace(':', '-').upper()
        interface_name = port_info["port"]["name"]
        if len(interface_name) < 1:
            interface_name = ''.join(
                random.choice(string.ascii_lowercase) for i in range(10))

        if not port_info["port"]["fixed_ips"]:
            raise Exception(
                "Not allowed to attach an interface without"
                "fixed ips")

        segment_uuid = ""
        for fixed_ip in port_info["port"]["fixed_ips"]:
            subnet_id = fixed_ip["subnet_id"]
            subnet_info = neutron.show_subnet(subnet_id)
            s_name = subnet_info["subnet"]["name"]
            subnet_names.append(
                s_name[:-3] if s_name.endswith('-V6') else s_name)

            address = fixed_ip["ip_address"]
            if ipaddress.ip_address(address).version == 4:
                ipv4s.append(address)
            else:
                ipv6s.append(address)

            if "segment_id" in subnet_info["subnet"]:
                other = subnet_info["subnet"]["segment_id"]
                if (segment_uuid and
                    len(segment_uuid) > 1 and
                    segment_uuid != other):
                    LOG.warning("Port has multiple segments.")
                segment_uuid = other

        # Check that we have a single subnet
        if len(set(subnet_names)) > 1:
            raise Exception(
                "Not allowed to have multiple subnet names on "
                "single interface")

        # Check that we have a single ipv4
        if len(ipv4s) > 1:
            raise Exception(
                "Not allowed to have multiple ip v4 addresses on "
                "single interface")

        # Check that we have a single ipv6
        if len(ipv6s) > 1:
            raise Exception(
                "Not allowed to have multiple ip v6 addresses on "
                "single interface")

        client_landb = LanDB()

        if CONF.cern.utils_attach_use_segments_instead_of_netcluster:
            service_name = subnet_names[0].replace("-V6", "")
            cluster_name = client_landb.cluster_by_service_host(
                instance.host, service_name)
        else:
            cluster_info = neutron.list_clusters()

            for cluster in cluster_info["clusters"]:
                if subnet_id in cluster["subnets"]:
                    cluster_name = cluster["name"]
                    break

        subnet_name = subnet_names[0]
        ipv4 = ipv4s[0] if len(ipv4s) > 0 else None
        ipv6 = ipv6s[0] if len(ipv6s) > 0 else None

        move_interface = "landb-move-interface" in port_info["port"]["tags"]

        client_landb.device_addcard(
            instance.display_name, mac_address)
        if move_interface:
            client_landb.device_moveinterface(
                instance.display_name, interface_name)
        else:
            client_landb.device_addinterface(
                instance.display_name, ipv4, ipv6, subnet_name,
                cluster_name, interface_name)

        client_landb.bind_unbind_interface(interface_name, mac_address)

        if len(port_info["port"]["name"]) < 1:
            neutron.update_port(
                    port_id, {'port': {'name': interface_name}})

    except Exception as e:
        LOG.error("Cannot add card/interface: %s", e)
        raise exception.CernInvalidHostname("Cannot add card/interface")


def dettach_interface(network_api, context, instance, port_id):
    try:
        neutron = Neutron(network_api, context)
        mac_address = None
        ip_address = None

        port_info = neutron.show_port(port_id)
        mac_address = port_info["port"]["mac_address"]
        mac_address = mac_address.replace(':', '-').upper()
        ip_address = port_info["port"]["fixed_ips"][0]["ip_address"]

        keep_interface = "landb-move-interface" in port_info["port"]["tags"]

        client_landb = LanDB()
        if not keep_interface:
            client_landb.device_removeinterface(
                instance.display_name, ip_address)
        client_landb.device_removecard(
            instance.display_name, mac_address)
    except Exception as e:
        LOG.error("Cannot remove card/interface: %s", e)
        raise exception.CernInvalidHostname("Cannot remove card/interface")


def deregister_landb_device(instance):
    """Deregister VMs in LanDB.

    This method is called in the API in case the BuildRequest creation fails.
    Or when a nova instance is being deleted.
    """
    # TODO(dfailing): Use python-landbclient
    landb = LanDB()

    device_name = instance.hostname
    if not device_name:
        device_name = instance.display_name

    cern_physical = instance.flavor.extra_specs.get('cern:physical')

    if not cern_physical:
        # Only do things for VMs
        LOG.debug(f"landb.vm_delete({device_name})")
        if not CONF.cern.landb_dry_run:
            landb.vm_delete(device_name)


def register_landb_device(context, instance, volume_meta=None):
    """Register VMs in LanDB and verify hostnames for ironic.

    This method is called in the API after the BuildRequests are created for
    all servers.
    """
    landb = LanDB()

    if volume_meta is None:
        volume_meta = {}

    if instance.hostname and instance.display_name:
        if instance.hostname.lower() != instance.display_name.lower():
            msg = ("Hostname and display_name of the VM differ. Please use"
                   " a valid hostname for the name")
            raise exc.HTTPBadRequest(explanation=msg)

    device_name = instance.hostname
    if not device_name:
        device_name = instance.display_name

    validate_instance(context, device_name, instance)

    cern_physical = instance.flavor.extra_specs.get('cern:physical')
    # TODO(dfailing) we verify in validate_instance already.
    # if landb.device_exists(device_name):
    #     msg = f"Hostname {device_name} already in use"
    #     raise exc.HTTPBadRequest(explanation=msg)

    # if cern_physical:
    # TODO(dfailing): we now verify the name 3 times?????
    # This method is called before the scheduling decision.
    # Here we don't know which server will be assigned for this server,
    # thus we cannot rename the interface.
    # We still make sure the hostame is free with the previous call
    # for validate_instance
    if not cern_physical:
        # VM
        # Get some metadata fields
        metadata = instance.metadata
        project_metadata = Keystone(context)._get_project(
            instance.project_id).__dict__

        responsible = None
        mainuser = None
        description = ''
        ipv6ready = False

        if 'landb-description' in metadata:
            description = metadata['landb-description']

        if 'landb-ipv6ready' in metadata:
            ipv6ready = metadata['landb-ipv6ready'].lower() == 'true'

        xldap = Xldap()
        if 'landb-responsible' in metadata:
            responsible = _get_landb_user_group(
                metadata['landb-responsible'], xldap)
        elif 'landb-responsible' in project_metadata:
            responsible = _get_landb_user_group(
                project_metadata['landb-responsible'], xldap)
        else:
            responsible = _get_landb_user_group(instance.user_id, xldap)

        if 'landb-mainuser' in metadata:
            mainuser = _get_landb_user_group(metadata['landb-mainuser'], xldap)
        elif 'landb-mainuser' in project_metadata:
            mainuser = _get_landb_user_group(
                project_metadata['landb-mainuser'], xldap)
        else:
            mainuser = _get_landb_user_group(instance.user_id, xldap)

        # Get OS
        LOG.debug("Instance %r", instance)
        LOG.debug("Instance.system_metadata: %r", instance.system_metadata)
        LOG.debug("volume_meta: %r", volume_meta)

        os = {'Name': 'UNKNOWN', 'Version': 'UNKNOWN'}
        if 'image_os' in instance.system_metadata:
            os['Name'] = instance.system_metadata['image_os']

        if 'landb-os' in instance.metadata:
            os['Name'] = instance.metadata['landb-os']

        if 'image_os_version' in instance.system_metadata:
            os['Version'] = instance.system_metadata['image_os_version']

        if 'landb-osversion' in instance.metadata:
            os['Version'] = instance.metadata['landb-osversion']

        LANDB_MANAGER = {'FirstName': 'E-GROUP',
                         'Name': CONF.cern.vm_landb_manager,
                         'Department': 'IT'}

        LOG.debug("landb.device_create(%r", dict(
            device_name = device_name,
            location = VM_LOCATION,
            manufacturer = VM_MANUFACTURER,
            model = VM_MODEL,
            tag = VM_TAG,
            manager = LANDB_MANAGER,
            mainuser = mainuser,
            responsible = responsible,
            description = description,
            os = os,
            ipv6ready = ipv6ready,
            manager_locked = True
        ))
        if not CONF.cern.landb_dry_run:
            landb.device_create(
                device_name = device_name,
                location = VM_LOCATION,
                manufacturer = VM_MANUFACTURER,
                model = VM_MODEL,
                tag = VM_TAG,
                manager = LANDB_MANAGER,
                mainuser = mainuser,
                responsible = responsible,
                description = description,
                os = os,
                ipv6ready = ipv6ready,
                manager_locked = True
            )


def _get_landb_user_group(user_or_egroup, client_xldap):
    user_id = client_xldap.user_exists(user_or_egroup)
    egroup_id = client_xldap.egroup_exists(user_or_egroup)
    if user_id:
        landb_mainuser = {'PersonID': user_id}
    elif egroup_id:
        landb_mainuser = {'FirstName': 'E-GROUP', 'Name': egroup_id}
    else:
        LOG.error("Cannot find user/egroup")
        raise exception.CernInvalidUserEgroup()

    return landb_mainuser


def describe_availaility_zones(context):
    result = []
    ctxt = context.elevated()
    zones = get_availaility_zones(ctxt)
    for zone in zones:
        if zone in CONF.cern.hide_availability_zones:
            continue
        result.append({'zoneName': zone,
                       'zoneState': {'available': True},
                       "hosts": None})
    return {'availabilityZoneInfo': result}


def get_availaility_zones(context):
    zones = set()
    aggs = objects.AggregateList.get_by_metadata_key(
        context, 'availability_zone')
    for agg in aggs:
        zones.add(agg.availability_zone)
    return zones


def update_landb_on_show(context, instance, req):
    if (instance.vm_state in [
            vm_states.ERROR,
            vm_states.SOFT_DELETED,
            vm_states.DELETED,
            vm_states.BUILDING]):
        return

    if ('HTTP_USER_AGENT' in req.environ.keys() and
            'gopher' not in req.environ['HTTP_USER_AGENT'] and
            'svcprobe' not in context.user_id and
            'svcneu' not in context.user_id):
        client_landb = LanDB()
        try:
            instance_name = instance.hostname.upper()
            instance_node = instance.node.upper().strip('.CERN.CH')
            vm_landb = client_landb.vmGetInfo(instance_name)
            if (vm_landb.Name.upper() == instance_name and
                    vm_landb.IsVM and vm_landb.VMParent and instance.node):
                if (instance_node != vm_landb.VMParent.upper()):
                    LOG.info("Updating VM parent of instance: %s",
                             instance.uuid)
                    client_landb.vm_migrate(
                        instance_name,
                        instance_node)
        except Exception:
            LOG.warning("Failed to update VM parent")


def validate_instance(context, name, server_dict):
    instance_metadata = server_dict.get('metadata', {})
    validate_landb_hostname(name)
    validate_landb_device(name, instance_metadata)
    validate_windows_hostname(context, name, server_dict,
                              instance_metadata)
    validate_metadata(context, instance_metadata)
    checkdns(name, instance_metadata)


def validate_landb_hostname(name):
    if len(name) > 64:
        msg = "Instance name too long"
        raise exc.HTTPBadRequest(explanation=msg)

    # TODO(dfailing) maybe just replace with regex
    # possibility: [a-zA-Z][a-zA-Z0-9\-]*[a-zA-Z0-9]
    # alternative: nova.utils.sanitize_hostname(name) != name
    if utils.sanitize_hostname(name).lower() != name.lower():
        msg = "Instance name is not a valid hostname"
        raise exc.HTTPBadRequest(explanation=msg)

    if name[0].isdigit():
        msg = "Instance name is not a valid hostname"
        raise exc.HTTPBadRequest(explanation=msg)

    if '.' in name:
        msg = "Instance name is not a valid hostname"
        raise exc.HTTPBadRequest(explanation=msg)

    if '_' in name:
        msg = "Instance name is not a valid hostname"
        raise exc.HTTPBadRequest(explanation=msg)


def validate_landb_device(name, metadata):
    if check_flag_not_present_or_value(metadata, 'cern-checklandbdevice'):
        if LanDB().device_exists(name):
            msg = "Hostname already in use"
            raise exc.HTTPBadRequest(explanation=msg)


def validate_windows_hostname(context, name, server_dict, instance_metadata):
    image_service = glance.API()
    volume_service = cinder.API()
    os = None

    try:
        if 'landb-os' in instance_metadata.keys():
            os = (instance_metadata['landb-os']).lower()
        elif ('imageRef' in server_dict.keys() and
                server_dict['imageRef'] != ''):
            img_metadata = image_service.get(context, server_dict['imageRef'])
            os = get_image_os(img_metadata)
        elif 'block_device_mapping_v2' in server_dict.keys():
            for i in server_dict['block_device_mapping_v2']:
                if ('boot_index' in i.keys() and
                        int(i['boot_index']) == 0 and
                        'source_type' in i.keys()):
                    if i['source_type'] == 'image':
                        img_metadata = image_service.get(context,
                                                         i['image_id'])
                        os = get_image_os(img_metadata)
                        break
                    elif i['source_type'] == 'volume':
                        vol_metadata = volume_service.get(context,
                                                        i['volume_id'])
                        os = get_volume_os(vol_metadata)
                        break
        elif 'block_device_mapping' in server_dict.keys():
            for i in server_dict['block_device_mapping']:
                vol_metadata = volume_service.get(context, i['volume_id'])
                os = get_volume_os(vol_metadata)
                break
    except Exception:
        msg = ("API error. Please contact the Cloud Team support.")
        raise exc.HTTPBadRequest(explanation=msg)

    if os == 'windows' and len(name) > 15:
        msg = ("Instance name too long. Windows images only support "
            "15 character hostname")
        raise exc.HTTPBadRequest(explanation=msg)


def get_image_os(img_metadata):
    if ('properties' in img_metadata.keys() and
            'os' in img_metadata['properties'].keys()):
        return img_metadata['properties']['os'].lower()
    return None


def get_volume_os(vol_metadata):
    if ('volume_image_metadata' in vol_metadata.keys() and
            'os' in vol_metadata['volume_image_metadata'].keys()):
        return vol_metadata['volume_image_metadata']['os'].lower()
    return None


def validate_metadata(context, metadata):
    client_landb = LanDB()
    client_xldap = Xldap()
    if ('landb-responsible' in metadata.keys() and
            metadata['landb-responsible'] != ''):
        user_id = client_xldap.user_exists(metadata['landb-responsible'])
        egroup_id = client_xldap.egroup_exists(metadata['landb-responsible'])
        if user_id or egroup_id:
            pass
        else:
            msg = "Cannot find user/egroup for responsible user"
            raise exc.HTTPBadRequest(explanation=msg)

    if ('landb-mainuser' in metadata.keys() and
            metadata['landb-mainuser'] != ''):
        user_id = client_xldap.user_exists(metadata['landb-mainuser'])
        egroup_id = client_xldap.egroup_exists(metadata['landb-mainuser'])
        if user_id or egroup_id:
            pass
        else:
            msg = "Cannot find user/egroup for responsible user"
            raise exc.HTTPBadRequest(explanation=msg)

    if 'landb-alias' in metadata.keys():
        # We can use empty hostname here, because we do not care about
        # the hostname
        intf_alias_dict = _parse_landb_alias_and_set_string(
                metadata['landb-alias'], '')
        for alias in reduce(set.union, intf_alias_dict.values()):
            try:
                # validate alias only for format first
                alias_clean = alias.lower().split("--load-")[0]
                alias_clean = alias_clean.split(".cern.ch")[0]
                validate_landb_hostname(alias_clean)
                # check if the alias is already in LanDB
                if client_landb.device_exists(alias):
                    raise
            except Exception:
                msg = ("Alias - %s - The device already exists or is not "
                       "a valid hostname" % str(alias))
                raise exc.HTTPBadRequest(explanation=msg)

        _validate_alias_load(reduce(set.union, intf_alias_dict.values()),
                        context, client_landb)

    if 'landb-set' in metadata.keys():
        # We can use empty hostname here, because we do not care about
        # the hostname
        intf_set_dict = _parse_landb_alias_and_set_string(
                metadata['landb-set'], '')
        try:
            _validate_set_membership(
                reduce(set.union, intf_set_dict.values()),
                context.project_id, client_landb)
        except exception.CernSetNoPermission as ex:
            msg = ("Wrong permissions to use the set %s" % str(ex.message))
            raise exc.HTTPBadRequest(explanation=msg)


def checkdns(name, metadata):
    if check_flag_not_present_or_value(metadata, 'cern-checkdns'):
        if Dns().gethostbyname(name):
            msg = "Hostname already in DNS. Wait for DNS refresh"
            raise exc.HTTPBadRequest(explanation=msg)


def validate_rebuild(rebuild_dict):
    if 'name' in rebuild_dict:
        raise exc.HTTPBadRequest(explanation="Hostname cannot be updated.")


def terminate_instance(instance):
    if check_flag_not_present_or_value(
            instance.metadata, 'cern-activedirectory'):
        ActiveDirectory().delete(str(instance['hostname']))


def update_port_request(instance, port_req_body):
    port_req_body['port']['device_owner'] = (
        'compute:%s' % instance.availability_zone)
    port_req_body['port']['binding:host_id'] = instance.host

    # In BFV, we are passing os information as of extra_dhcp options
    if not instance.image_ref:
        dhcp_opts = []
        system_meta = utils.instance_sys_meta(instance)
        img_meta = utils.get_image_from_system_metadata(system_meta)
        for opt in ['os', 'os_version']:
            if img_meta and opt in img_meta['properties']:
                dhcp_opts.append(
                    {'opt_name': opt, 'opt_value': img_meta['properties'][opt]}
                )
        port_req_body['port']['extra_dhcp_opts'] = dhcp_opts
    return port_req_body


def update_port_with_subnet(instance, port_req_body, port_client):
    # Find the most available subnets on port creation
    host_subnets = port_client.show_host(instance.host)
    subnet_id = host_subnets['host']['most_available_subnet_v4']
    port_req_body['port']['fixed_ips'] = [
        {'subnet_id': subnet_id}]
    if host_subnets['host']['most_available_subnet_v6']:
        subnet_id_v6 = host_subnets['host']['most_available_subnet_v6']
        port_req_body['port']['fixed_ips'] += [
            {'subnet_id': subnet_id_v6}]
    return port_req_body


def sort_subnets(subnets):
    try:
        subnets = sorted(subnets, key=lambda k: k['version'])
    except Exception:
        LOG.error("Cannot sort subnets by IP version")
        subnets = subnets
    return subnets


def _validate_set_membership(setlist, project_id, client_landb):
    for set_name in setlist:
        projects = client_landb.get_projects_set(set_name)
        if project_id not in projects:
            LOG.error("The lanDB Set doesn't exist or this "
                      "OpenStack Project can't add/remove "
                      "address in the Set: %s", set_name)
            raise exception.CernSetNoPermission()


def _validate_alias_load(alias_list, context, client_landb):
    devices_landb = set()
    for alias in alias_list:
        if (('--LOAD-' in alias) and
                (alias.split('--LOAD-')[0].strip() != '') and
                ((alias.split('.CERN.CH')[0])[-1] == '-')):
            devices_landb.update(client_landb.lb_consistency(alias))

    if devices_landb:
        instances_project = (
            objects.InstanceMappingList.get_by_project_id(
                context, context.project_id)
        )
        instances_name = set()
        for i in instances_project:
            with nova_context.target_cell(
                    context, i.cell_mapping) as cctxt:
                try:
                    temp = objects.Instance.get_by_uuid(
                        cctxt, i.instance_uuid)
                    instances_name.add(
                        ((temp).display_name).upper())
                except Exception:
                    LOG.debug("Instance not found: %s",
                              i.instance_uuid)
                    pass
        if bool(devices_landb - instances_name):
            LOG.error(
                "LB --LOAD- devices are not in the "
                "user project: %s",
                (devices_landb - instances_name))
            raise exception.CernInvalidLB4Device()


def _parse_landb_alias_and_set_string(raw_string, hostname):
    parsed_dict = {}
    for value in raw_string.split(';'):
        ifname = hostname + ".CERN.CH"
        if ':' in value:
            ifname = value.split(':')[0].strip()
            ifname = ifname if '.' in ifname else ifname + ".CERN.CH"
            value = value.split(':')[1]

        entries = set(v.strip().upper() for v in value.split(','))
        if ifname.upper() in parsed_dict:
            entries.update(parsed_dict[ifname.upper()])

        # Do not keep empty entries
        if '' in entries:
            entries.remove('')
        parsed_dict[ifname.upper()] = entries
    return parsed_dict


def delete_instance_metadata(context, instance, key):
    _metadata = dict(instance.metadata)

    landb_update = False
    landb_os = 'UNKNOWN'
    landb_osversion = 'UNKNOWN'
    landb_description = None
    landb_responsible = None
    landb_mainuser = None

    client_xldap = Xldap()
    client_key = Keystone(context)
    client_landb = LanDB()
    image_metadata = get_image_metadata(context, instance)
    is_ironic = False
    if not CONF.cern.landb_dry_run:
        if client_landb.isIronic(instance.display_name):
            is_ironic = True

    if key.startswith('landb-alias'):
        _delete_instance_landb_alias(
            instance, key, _metadata, client_landb, is_ironic)

    if key == 'landb-ipv6ready':
        LOG.debug(
            f"client_landb.ipv6ready_update({instance['hostname']}, False)")
        if not CONF.cern.landb_dry_run:
            client_landb.ipv6ready_update(instance['hostname'], False)

    if key == 'landb-internet-connectivity':
        LOG.debug(
            f"client_landb.internet_update({instance['hostname']}, True)")
        if not CONF.cern.landb_dry_run:
            client_landb.internet_update(instance['hostname'], True)

    if key == 'landb-set':
        if 'landb-set' in _metadata.keys():
            intf_set_dict = _parse_landb_alias_and_set_string(
                    _metadata['landb-set'], instance['hostname'])

            # Validate
            try:
                _validate_set_membership(
                        reduce(set.union, intf_set_dict.values()),
                        context.project_id, client_landb)
            except exception.CernSetNoPermission:
                LOG.error("Ignoring validation of sets in delete???")

            # remove the sets
            for ifname, set_names in intf_set_dict.items():
                for set_name in set_names:
                    LOG.debug("client_landb.delete_address_set("
                        f"{instance['hostname']}, {set_name}, {ifname})")
                    if not CONF.cern.landb_dry_run:
                        client_landb.delete_address_set(
                            instance['hostname'], set_name, ifname)

    if key == 'landb-os':
        landb_update = True
        if ('properties' in image_metadata.keys() and
                'os' in image_metadata['properties'].keys()):
            landb_os = image_metadata['properties']['os']
        if 'landb-osversion' in _metadata.keys():
            landb_osversion = _metadata['landb-osversion']
    elif key == 'landb-osversion':
        landb_update = True
        if ('properties' in image_metadata.keys() and
                'os_version' in image_metadata['properties'].keys()):
            landb_osversion = image_metadata['properties']['os_version']
        if 'landb-os' in _metadata.keys():
            landb_os = _metadata['landb-os']
    else:
        if ('properties' in image_metadata.keys() and
                'os' in image_metadata['properties'].keys()):
            landb_os = image_metadata['properties']['os']
        if 'landb-os' in _metadata.keys():
            landb_os = _metadata['landb-os']
        if ('properties' in image_metadata.keys() and
                'os_version' in image_metadata['properties'].keys()):
            landb_osversion = image_metadata['properties']['os_version']
        if 'landb-osversion' in _metadata.keys():
            landb_osversion = _metadata['landb-osversion']
    landb_operating_system = {'Name': landb_os,
                              'Version': landb_osversion}

    if key == 'landb-mainuser':
        landb_update = True
        project_mainuser = client_key.get_project_mainuser(
            instance['project_id'])
        if project_mainuser:
            user_id = client_xldap.user_exists(project_mainuser)
            egroup_id = client_xldap.egroup_exists(project_mainuser)
            if user_id:
                landb_mainuser = {'PersonID': user_id}
            elif egroup_id:
                landb_mainuser = {'FirstName': 'E-GROUP', 'Name': egroup_id}
            else:
                LOG.error("Cannot find user/egroup for main user. %s",
                          egroup_id)
                raise exception.CernInvalidUserEgroup()
        else:
            user_id = client_xldap.user_exists(instance['user_id'])
            if user_id:
                landb_mainuser = {'PersonID': user_id}
            else:
                LOG.error("Cannot find user/egroup for main user. %s",
                          user_id)
                raise exception.CernInvalidUserEgroup()

    if key == 'landb-responsible':
        landb_update = True
        project_responsible = client_key.get_project_responsible(
            instance['project_id'])
        if project_responsible:
            user_id = client_xldap.user_exists(project_responsible)
            egroup_id = client_xldap.egroup_exists(project_responsible)
            if user_id:
                landb_responsible = {'PersonID': user_id}
            elif egroup_id:
                landb_responsible = {'FirstName': 'E-GROUP', 'Name': egroup_id}
            else:
                LOG.error("Cannot find user/egroup for main user. %s",
                          egroup_id)
                raise exception.CernInvalidUserEgroup()
        else:
            user_id = client_xldap.user_exists(instance['user_id'])
            if user_id:
                landb_responsible = {'PersonID': user_id}
            else:
                LOG.error("Cannot find user/egroup for main user. %s",
                          user_id)
                raise exception.CernInvalidUserEgroup()

    if key == 'landb-description':
        landb_update = True
        landb_description = ''

    if landb_update:
        if is_ironic:
            client_landb.device_update(
                instance['hostname'],
                description=landb_description,
                operating_system=landb_operating_system,
                responsible_person=landb_responsible,
                user_person=landb_mainuser)
        else:
            LOG.debug("client_landb.vm_update(%r", dict(
                name=instance['hostname'],
                description=landb_description,
                operating_system=landb_operating_system,
                responsible_person=landb_responsible,
                user_person=landb_mainuser))
            if not CONF.cern.landb_dry_run:
                client_landb.vm_update(
                    instance['hostname'],
                    description=landb_description,
                    operating_system=landb_operating_system,
                    responsible_person=landb_responsible,
                    user_person=landb_mainuser)


def _delete_instance_landb_alias(instance, key, _metadata, client_landb,
                                 is_ironic):
    new_alias_metadata = ";".join(
        [v for k, v in _metadata.items()
            if k.startswith('landb-alias') and k != key])
    old_alias_metadata = ";".join(
        [v for k, v in _metadata.items()
            if k.startswith('landb-alias')])

    main_interface = instance['hostname'].upper() + ".CERN.CH"
    intf_alias_dict = _parse_landb_alias_and_set_string(
            new_alias_metadata, instance['hostname'])
    old_intf_alias_dict = _parse_landb_alias_and_set_string(
            old_alias_metadata, instance['hostname'])

    # Add serial number alias for ironic devices
    if is_ironic:
        device_info = client_landb.getDeviceInfo(instance['hostname'])
        if main_interface in intf_alias_dict:
            intf_alias_dict[main_interface].add(device_info.SerialNumber)
        else:
            intf_alias_dict[main_interface] = set(
                [device_info.SerialNumber])

        ipmi_interface = instance['hostname'].upper() + "-IPMI.CERN.CH"
        if ipmi_interface in intf_alias_dict:
            # Also for the IPMI interface
            intf_alias_dict[main_interface].add(
                device_info.SerialNumber + "-IPMI")

    # Add .CERN.CH if not present
    for interface, temp_alias in intf_alias_dict.items():
        intf_alias_dict[interface] = set(map(
            lambda v: v if v.endswith('.CERN.CH') else v + '.CERN.CH',
            temp_alias))

    # (Leave out validation for DNS A, since we only want to delete)
    # Also iterate over the old keys to make sure we delete the old ones
    for ifname in old_intf_alias_dict.keys():
        if ifname not in intf_alias_dict:
            intf_alias_dict[ifname] = set()

    # Perform the alias update
    for ifname, alias in intf_alias_dict.items():
        LOG.debug("client_landb.alias_update("
                f"{instance['hostname']}, {ifname}, {alias}")
        if not CONF.cern.landb_dry_run:
            client_landb.alias_update(
                instance['hostname'], ifname, list(alias))


def update_instance_metadata(    # noqa: C901
        context, instance, metadata, _metadata):
    landb_update = False
    landb_os = 'UNKNOWN'
    landb_osversion = 'UNKNOWN'
    landb_description = None
    landb_responsible = None
    landb_mainuser = None

    client_landb = LanDB()
    is_ironic = False
    if not CONF.cern.landb_dry_run:
        if client_landb.isIronic(instance.display_name):
            is_ironic = True

    client_xldap = Xldap()
    image_metadata = get_image_metadata(context, instance)

    update_landb_alias = len(
        [v for k, v in metadata.items()
           if k.startswith('landb-alias')]) > 0

    if update_landb_alias:
        new_metadata = dict(instance.metadata)
        new_metadata.update(metadata)

        new_alias_metadata = ";".join(
            [v for k, v in new_metadata.items()
               if k.startswith('landb-alias')])
        main_interface = instance['hostname'].upper() + ".CERN.CH"
        intf_alias_dict = _parse_landb_alias_and_set_string(
                new_alias_metadata, instance['hostname'])

        # Add serial number alias for ironic devices
        if is_ironic:
            device_info = client_landb.getDeviceInfo(instance['hostname'])
            if main_interface in intf_alias_dict:
                intf_alias_dict[main_interface].add(device_info.SerialNumber)
            else:
                intf_alias_dict[main_interface] = set(
                    [device_info.SerialNumber])

            ipmi_interface = instance['hostname'].upper() + "-IPMI.CERN.CH"
            if ipmi_interface in intf_alias_dict:
                # Also for the IPMI interface
                intf_alias_dict[main_interface].add(
                    device_info.SerialNumber + "-IPMI")

        # Add .CERN.CH if not present
        for interface, temp_alias in intf_alias_dict.items():
            intf_alias_dict[interface] = set(map(
                lambda v: v if v.endswith('.CERN.CH') else v + '.CERN.CH',
                temp_alias))

        # validate A entries as LB
        _validate_alias_load(reduce(set.union, intf_alias_dict.values()),
                        context, client_landb)

        # Perform the alias update
        for ifname, alias in intf_alias_dict.items():
            LOG.debug("client_landb.alias_update("
                    f"{instance['hostname']}, {ifname}, {alias}")
            if not CONF.cern.landb_dry_run:
                client_landb.alias_update(
                    instance['hostname'], ifname, list(alias))

    if 'landb-ipv6ready' in metadata.keys():
        if metadata['landb-ipv6ready'].lower() == 'true':
            LOG.debug(f"client_landb.ipv6ready_update({instance['hostname']},"
                       " True)")
            if not CONF.cern.landb_dry_run:
                client_landb.ipv6ready_update(instance['hostname'], True)
        else:
            LOG.debug(f"client_landb.ipv6ready_update({instance['hostname']},"
                       " False)")
            if not CONF.cern.landb_dry_run:
                client_landb.ipv6ready_update(instance['hostname'], False)

    if 'landb-internet-connectivity' in metadata.keys():
        internet_dict = _parse_landb_alias_and_set_string(
                metadata['landb-internet-connectivity'], instance['hostname'])
        for ifname, set_values in internet_dict.items():
            for value in set_values:
                if value.lower() == 'false':
                    LOG.debug("client_landb.internet_update("
                        f"{instance['hostname']}, {ifname}, False)")
                    if not CONF.cern.landb_dry_run:
                        client_landb.internet_update(
                            instance['hostname'], ifname, False)
                else:
                    LOG.debug("client_landb.internet_update("
                        f"{instance['hostname']}, {ifname}, True)")
                    if not CONF.cern.landb_dry_run:
                        client_landb.internet_update(
                            instance['hostname'], ifname, True)

    if 'landb-set' in metadata.keys():
        new_set_dict = {}
        old_set_dict = {}

        # parse the old data
        if 'landb-set' in dict(instance.metadata).keys():
            old_set_dict = _parse_landb_alias_and_set_string(
                    dict(instance.metadata)['landb-set'],
                    instance['hostname'])

        new_set_dict = _parse_landb_alias_and_set_string(
                metadata['landb-set'], instance['hostname'])

        try:
            # validate also the old set, but only warn in our logs
            _validate_set_membership(
                    reduce(set.union, old_set_dict.values()),
                    context.project_id, client_landb)
        except Exception:
            LOG.warning("LanDB verifying old set values failed, "
                    "but ignoring error")

        _validate_set_membership(
                reduce(set.union, new_set_dict.values()),
                context.project_id, client_landb)

        # If we remove the old ones, make sure that they get deleted
        for old_ifname in old_set_dict.keys():
            if old_ifname not in new_set_dict:
                new_set_dict[old_ifname] = set()

        for ifname, new_set_names in new_set_dict.items():
            old_set_names = set()
            if ifname in old_set_dict:
                old_set_names = old_set_dict[ifname]

            # remove deleted sets
            for deleted_set in (old_set_names - new_set_names):
                LOG.debug("client_landb.delete_address_set("
                    f"{instance['hostname']}, {deleted_set}, {ifname})")
                if not CONF.cern.landb_dry_run:
                    client_landb.delete_address_set(
                        instance['hostname'], deleted_set, ifname)
            # insert new sets
            for new_set in (new_set_names - old_set_names):
                LOG.debug("client_landb.insert_address_set("
                    f"{instance['hostname']}, {new_set}, {ifname})")
                if not CONF.cern.landb_dry_run:
                    client_landb.insert_address_set(
                        instance['hostname'], new_set, ifname)

    if 'landb-os' in metadata.keys():
        landb_update = True
        landb_os = metadata['landb-os']
    elif 'landb-os' in _metadata.keys():
        landb_os = _metadata['landb-os']
    elif ('properties' in image_metadata.keys() and
            'os' in image_metadata['properties'].keys()):
        landb_os = image_metadata['properties']['os']

    if 'landb-osversion' in metadata.keys():
        landb_update = True
        landb_osversion = metadata['landb-osversion']
    elif 'landb-osversion' in _metadata.keys():
        landb_osversion = _metadata['landb-osversion']
    elif ('properties' in image_metadata.keys() and
            'os_version' in image_metadata['properties'].keys()):
        landb_osversion = image_metadata['properties']['os_version']

    landb_operating_system = {'Name': landb_os,
                              'Version': landb_osversion}

    if 'landb-mainuser' in metadata.keys():
        landb_update = True
        user_id = client_xldap.user_exists(metadata['landb-mainuser'])
        egroup_id = client_xldap.egroup_exists(metadata['landb-mainuser'])
        if user_id:
            landb_mainuser = {'PersonID': user_id}
        elif egroup_id:
            landb_mainuser = {'FirstName': 'E-GROUP', 'Name': egroup_id}
        else:
            LOG.error("Cannot find user/egroup for main user")
            raise exception.CernInvalidUserEgroup()

    if 'landb-responsible' in metadata.keys():
        landb_update = True
        user_id = client_xldap.user_exists(metadata['landb-responsible'])
        egroup_id = client_xldap.egroup_exists(metadata['landb-responsible'])
        if user_id:
            landb_responsible = {'PersonID': user_id}
        elif egroup_id:
            landb_responsible = {'FirstName': 'E-GROUP', 'Name': egroup_id}
        else:
            LOG.error("Cannot find user/egroup for responsible user")
            raise exception.CernInvalidUserEgroup()

    if 'landb-description' in metadata.keys():
        landb_update = True
        landb_description = metadata['landb-description']

    if landb_update:
        if is_ironic:
            client_landb.device_update(
                instance['hostname'],
                description=landb_description,
                operating_system=landb_operating_system,
                responsible_person=landb_responsible,
                user_person=landb_mainuser)
        else:
            LOG.debug("client_landb.vm_update(%r", dict(
                name=instance['hostname'],
                description=landb_description,
                operating_system=landb_operating_system,
                responsible_person=landb_responsible,
                user_person=landb_mainuser))
            if not CONF.cern.landb_dry_run:
                client_landb.vm_update(
                    instance['hostname'],
                    description=landb_description,
                    operating_system=landb_operating_system,
                    responsible_person=landb_responsible,
                    user_person=landb_mainuser)


def build_instance(context, network_info, instance):
    if network_info is not None:
        network_info.wait(do_raise=True)
    instance.info_cache.network_info = network_info
    instance.save()

    if instance['hostname'] == "server-" + str(instance['uuid']):
        return
    if instance['hostname'].startswith('server-r-'):
        return
    meta = utils.instance_meta(instance)

    if (CONF.cern.enable_wait_dhcp and
            check_flag_not_present_or_value(meta, 'cern-waitdhcp')):
        wait_for_dhcp(instance)

    if check_flag_present_and_value(meta, 'cern-services', 'false'):
        return

    if check_flag_not_present_or_value(meta, 'cern-activedirectory'):
        register_activedirectory(context, instance)

    if check_flag_not_present_or_value(meta, 'cern-waitdns'):
        wait_for_dns(instance)


def wait_for_dns(instance):
    wait_time = 0
    while (wait_time < CONF.cern.wait_dns_time):
        if Dns().gethostbyname(str(instance['hostname'])):
            break

        LOG.info("Waiting for DNS - %s - %d secs",
                 instance['uuid'], wait_time)
        time.sleep(15)
        wait_time = wait_time + 15
    else:
        LOG.error("DNS update failed - %s", instance['uuid'])
        raise exception.CernDNS()


def wait_for_dhcp(instance):
    wait_time = 0
    while (wait_time < CONF.cern.wait_dhcp_time):
        LOG.info("Waiting for DHCP sync for instance %s - %d secs",
                 instance['uuid'], wait_time)
        time.sleep(30)
        wait_time = wait_time + 30


def get_image_metadata(context, instance):
    image_system_meta = {}
    # In case of boot from volume, image_id_or_uri may be None or ''
    if instance.image_ref:
        # If the base image is still available, get its metadata
        try:
            img = glance.API().get(context, instance.image_ref)
        except (nova_exception.ImageNotAuthorized,
                nova_exception.ImageNotFound,
                nova_exception.Invalid) as e:
            LOG.warning("Can't access image %(image_id)s: %(error)s",
                        {"image_id": instance.image_ref, "error": e},
                        instance=instance)
        else:
            flavor = instance.get_flavor()
            image_system_meta = utils.get_system_metadata_from_image(img,
                                                                     flavor)

    # Get the system metadata from the instance
    system_meta = utils.instance_sys_meta(instance)

    # Merge the metadata from the instance with the image's, if any
    system_meta.update(image_system_meta)

    # Convert the system metadata to image metadata
    return utils.get_image_from_system_metadata(system_meta)


def register_activedirectory(context, instance):
    image_metadata = get_image_metadata(context, instance)
    os = get_metadata_os(instance, image_metadata)

    if (os['Name'].upper() in ['LINUX', 'WINDOWS']):
        ActiveDirectory().register(str(instance['hostname']))
    else:
        LOG.info('Not registering: %s in AD becasue OS name: %s',
                 str(instance['hostname']), os['Name'])


def get_metadata_os(instance, image_metadata):
    try:
        os_name = 'UNKNOWN'
        os_version = 'UNKNOWN'
        if 'landb-os' in instance.metadata.keys():
            os_name = instance.metadata['landb-os']
        elif ('properties' in image_metadata.keys() and
                'os' in image_metadata['properties'].keys()):
            os_name = image_metadata['properties']['os']
        if 'landb-osversion' in instance.metadata.keys():
            os_version = instance.metadata['landb-osversion']
        elif ('properties' in image_metadata.keys() and
                'os_version' in image_metadata['properties'].keys()):
            os_version = image_metadata['properties']['os_version']
    except Exception as e:
        LOG.error("Can't get instance metadata os: %s", e)
        raise exception.CernLanDB()
    return {'Name': os_name, 'Version': os_version}


def allocate_network_ironic(context, instance):
    network_info = network_model.NetworkInfo()
    if ('landb-ironicskip' in instance.metadata.keys() and
            instance.metadata['landb-ironicskip'].lower() == 'true'):

        return network_info
    else:
        try:
            client_landb = LanDB()
            client_xldap = Xldap()
            client_key = Keystone(context)
            client_ironic = ironic_wrapper.IronicClientWrapper()

            bare_node = client_ironic.call(
                'node.get', instance.node,
                fields=('uuid', 'driver_info', 'instance_uuid'))

            if (bare_node.instance_uuid is not None):
                raise Exception(
                    "Ironic node seems occupied, not changing LanDB!")

            bare_ipmiaddress = bare_node.driver_info['ipmi_address']
            bare_hostname = client_landb.device_hostname(bare_ipmiaddress)
            image_os = get_metadata_os(instance,
                                       get_image_metadata(context, instance))
            metadata = client_landb.get_metadata(
                instance, client_xldap, client_key)

            client_landb.device_update(
                bare_hostname,
                new_device=instance.display_name,
                description=metadata['description'],
                operating_system=image_os,
                responsible_person=metadata['responsible'],
                user_person=metadata['mainuser'],
                ipv6ready=metadata['ipv6ready'])

            return network_info
        except Exception:
            LOG.error("Can't create Ironic resource")
            raise nova_exception.BuildAbortException(
                instance_uuid=instance.uuid,
                reason='Failed to create Ironic resource')


def deallocate_network_ironic(instance):
    if check_flag_present_and_value(instance.metadata, 'landb-ironicskip'):
        return

    client_landb = LanDB()

    if 'landb-set' in instance.metadata:
        try:
            old_set_dict = _parse_landb_alias_and_set_string(
                instance.metadata['landb-set'], instance['hostname'])

            # remove deleted sets
            for ifname, iset in old_set_dict:
                for deleted_set in iset:
                    LOG.debug("client_landb.delete_address_set("
                        f"{instance['hostname']}, {deleted_set}, {ifname})")
                    if not CONF.cern.landb_dry_run:
                        client_landb.delete_address_set(
                            instance['hostname'], deleted_set, ifname)
        except Exception as exc:
            LOG.warning("Could not delete all sets from physical node %r", exc)

    client_landb.device_rename_ironic(instance.display_name)


def check_flag_present_and_value(meta, flag, value='true'):
    return (flag in meta.keys() and meta[flag].lower().strip() == value)


def check_flag_not_present_or_value(meta, flag, value='false'):
    return ((flag in meta.keys() and meta[flag].lower().strip() != value) or
            flag not in meta.keys())
