#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import ipaddress
import random
import string
import time

from suds.client import Client
from suds import WebFault
from suds.xsd.doctor import Import
from suds.xsd.doctor import ImportDoctor

import logging as pylog
from nova.cern import exception
import nova.conf
from oslo_log import log as logging


CONF = nova.conf.CONF
LOG = logging.getLogger(__name__)
pylog.getLogger('suds.client').setLevel(pylog.CRITICAL)


class LanDB(object):
    MAX_AGE = 3600  # number of seconds after client should refresh

    _client = None
    _created = 0

    def __init__(self, username=None, password=None, client=None):
        if client is not None:
            self.client = client
        else:
            self.client = self.__auth(username=username, password=password)

    def __auth(self, username=None, password=None):
        """Authenticates in landb"""
        if LanDB._client and ((time.time() - LanDB._created) < LanDB.MAX_AGE):
            return LanDB._client

        url = 'https://network.cern.ch/sc/soap/soap.fcgi?v=6&WSDL'
        imp = Import('http://schemas.xmlsoap.org/soap/encoding/')
        d = ImportDoctor(imp)
        client = Client(url, doctor=d)

        if username is None or password is None:
            username = CONF.cern.landb_username
            password = CONF.cern.landb_password

        try:
            token = client.service.getAuthToken(username, password, 'CERN')
            myheader = dict(Auth={'token': token})
            client.set_options(soapheaders=myheader)
        except Exception as e:
            LOG.error("Cannot authenticate in landb: %s", e)
            raise exception.CernLanDBAuthentication()

        LanDB._client = client
        LanDB._created = time.time()
        return client

    def vm_update(self, device, new_device=None,
            location=None, manufacter=None, model=None, description=None,
            tag=None, operating_system=None, responsible_person=None,
            user_person=None, ipv6ready=None):
        """Update vm metadata in landb"""
        metadata = None
        try:
            metadata = self.client.service.getDeviceBasicInfo(device.upper())
        except Exception:
            pass

        if new_device is None:
            new_device = device

        if location is None:
            location = {'Floor': '0', 'Room': '0', 'Building': '0'}

        if manufacter is None:
            manufacter = 'KVM'

        if model is None:
            model = 'VIRTUAL MACHINE'

        if description is None and metadata is not None:
            description = (metadata.Description
                           if metadata.Description is not None else '')

        if tag is None:
            tag = 'OPENSTACK VM'

        if operating_system is None and metadata is not None:
            operating_system = metadata.OperatingSystem

        if responsible_person is None and metadata is not None:
            responsible_person = metadata.ResponsiblePerson

        if user_person is None and metadata is not None:
            user_person = metadata.UserPerson

        if ipv6ready is None and metadata is not None:
            ipv6ready = metadata.IPv6Ready

        manager_person = {'FirstName': 'E-GROUP',
                          'Name': 'ai-openstack-admin',
                          'Department': 'IT'}

        try:
            self.client.service.vmUpdate(device,
                        {'DeviceName': new_device,
                        'Location': location,
                        'Manufacturer': manufacter,
                        'Model': model,
                        'Description': description,
                        'Tag': tag,
                        'OperatingSystem': operating_system,
                        'ResponsiblePerson': responsible_person,
                        'LandbManagerPerson': manager_person,
                        'UserPerson': user_person,
                        'IPv6Ready': ipv6ready})
        except Exception as e:
            LOG.error("Cannot update landb: %s", e)
            raise exception.CernLanDBUpdate(e)

    def device_update(self, device, new_device=None,
            location=None, manufacter=None, model=None, description=None,
            operating_system=None, responsible_person=None, user_person=None,
            ipv6ready=None):
        """Update vm metadata in landb"""
        metadata = self.getDeviceInfo(device.upper())
        interface_rename = False
        if new_device is None:
            new_device = device
        else:
            interface_rename = True

        if description is None:
            description = (metadata.Description
                           if metadata.Description is not None else '')

        if operating_system is None:
            operating_system = metadata.OperatingSystem

        if responsible_person is None:
            responsible_person = metadata.ResponsiblePerson

        if user_person is None:
            user_person = metadata.UserPerson

        if ipv6ready is None:
            ipv6ready = metadata.IPv6Ready

        manager_person = {'FirstName': 'E-GROUP',
                          'Name': 'ai-openstack-admin',
                          'Department': 'IT'}

        try:
            self.client.service.deviceUpdate(device,
                        {'DeviceName': new_device,
                        'Location': metadata.Location,
                        'Zone': metadata.Zone,
                        'Manufacturer': metadata.Manufacturer,
                        'Model': metadata.Model,
                        'SerialNumber': metadata.SerialNumber,
                        'Description': description,
                        'Tag': 'OPENSTACK IRONIC NODE',
                        'OperatingSystem': operating_system,
                        'ResponsiblePerson': responsible_person,
                        'UserPerson': user_person,
                        'LandbManagerPerson': manager_person,
                        'IPv6Ready': ipv6ready,
                        'HCPResponse': 'true'})
        except Exception as e:
            LOG.error("Cannot update landb: %s", e)
            raise exception.CernLanDBUpdate(e)

        if interface_rename:
            for i in metadata.Interfaces:
                if i.IPAliases is not None:
                    for alias in i.IPAliases:
                        self.__unset_alias(i.Name, alias)
                if device.upper() + '-IPMI.CERN.CH' == i.Name.upper():
                    self.interface_rename(i.Name, new_device + '-IPMI.CERN.CH')
                    self.__set_alias(
                        new_device + '-IPMI.CERN.CH',
                        metadata.SerialNumber + '-IPMI.CERN.CH')
                elif device.upper() + '.CERN.CH' == i.Name.upper():
                    try:
                        self.interface_rename(i.Name, new_device + '.CERN.CH')
                    except Exception:
                        LOG.error("Can't rename interface: %s", i.Name)
                    try:
                        self.__set_alias(
                            new_device + '.CERN.CH',
                            metadata.SerialNumber + '.CERN.CH')
                    except Exception:
                        LOG.error("Can't set alias for interface: %s",
                                  new_device + '.CERN.CH')
                else:
                    LOG.warning("Device %s has multiple interfaces registered "
                                "in LanDB: %s",
                                device, i.Name)

    def device_rename_ironic(self, device, new_device=None):
        """Rename a device in LanDB and keep all names"""
        if new_device is None:
            for i in range(5):
                new_device = 'P' + ''.join(random.choice(
                    string.ascii_uppercase + string.digits) for x in range(10))
                LOG.debug("Random instance name for Landb: %s", new_device)
                if self.device_exists(new_device):
                    LOG.debug("Hostname already exists: %s", new_device)
                    continue
                else:
                    break

        os = {'Name': 'LINUX',
              'Version': 'UNKNOWN'}

        responsible = {'FirstName': 'E-GROUP',
                       'Name': 'AI-OPENSTACK-ADMIN',
                       'Department': 'IT'}

        user_person = {'FirstName': 'E-GROUP',
                       'Name': 'AI-OPENSTACK-ADMIN',
                       'Department': 'IT'}

        try:
            self.device_update(device,
                               new_device = new_device,
                               description = 'Not in use',
                               operating_system = os,
                               responsible_person = responsible,
                               user_person = user_person,
                               ipv6ready = True)
        except Exception as e:
            LOG.error("Cannot update device in landb: %s", e)
#            raise exception.CernLanDBUpdate()
            pass

    def vm_delete_v5_checks(self, device):
        # ONLY to handle v5 delete requests
        device_info = self.getDeviceInfo(device)
        interface_name = None
        for i in device_info.Interfaces:
            if i.Name.upper().endswith("-IPMI.CERN.CH"):
                continue
            if i.Name.upper() != device.upper() + ".CERN.CH":
                interface_name = i.Name.upper().split('.', 1)[0].strip()
                if device.upper() != interface_name:
                    LOG.error("Interface name is different from device: %s",
                              interface_name)
                    msg = ("%s - Interface name is different "
                           "from device" % str(interface_name))
                    raise exception.CernInvalidHostname(msg)
                self.interface_rename(
                    i.Name.upper(), device.upper() + ".CERN.CH")
            else:
                break

    def interface_rename(self, interface, new_interface):
        """Renames an interface"""
        try:
            self.client.service.interfaceRename(interface, new_interface)
        except Exception as e:
            LOG.warning("Cannot rename interface in lanDB: %s", e)
            return False
        return True

    def vmGetClusterMembership(self, device):
        """Get device cluster"""
        try:
            return self.client.service.vmGetClusterMembership(device)
        except Exception as e:
            LOG.error("Cannot get VM cluster: %s", e)
            raise exception.CernLanDB()

    def vmGetInfo(self, device):
        """Get vm device cluster"""
        try:
            return self.client.service.vmGetInfo(device)
        except Exception as e:
            LOG.warning("Cannot get VM info: %s", e)
            raise exception.CernLanDB()

    def getDeviceInfo(self, device):
        """Get device information"""
        try:
            return self.client.service.getDeviceInfo(device)
        except WebFault as ex:
            if "NOTFOUND#" in ex.fault.detail:
                LOG.warning(f"Device {device} not found in LanDB")
                raise exception.CernLanDBNotFound()
            else:
                LOG.error("Cannot get VM network info - %s", ex)
                raise exception.CernLanDB()
        except Exception as e:
            LOG.error("Cannot get VM network info - %s", e)
            raise exception.CernLanDB()

    def device_exists(self, device):
        """Check if a device is registered in landb"""
        try:
            if not self.client.service.searchDevice({'Name': device}):
                return False
        except Exception:
            return False
        return True

    def device_create(self, device_name, location, manufacturer, model, os,
                      responsible, description='', tag='', mainuser=None,
                      manager=None, ipv6ready=True, manager_locked=True,
                      serialnumber=''):
        """Create a device in landb.

        location shall be like {'Floor':'0', 'Room':'0', 'Building':'0'}
        os shall be {'Name':'LINUX', 'Version':'UNKNOWN'}
        responsible and mainuser shall be like:
          {'Name': 'Smith', 'FirstName': 'Alice', 'Department': 'IT',
           'Group': 'OIS', 'PersonID': 'id'}
            For a person {'PersonID': 123456} is enough
            For e-group {'FirstName': 'E-GROUP', 'Name': 'your-egroup-name'}
        """
        if manager_locked and not manager:
            manager = {'FirstName': 'E-GROUP',
                       'Name': 'AI-OPENSTACK-ADMIN',
                       'Department': 'IT'}

        device = {'DeviceName': device_name,
                  'Location': location,
                  'Manufacturer': manufacturer,
                  'Model': model,
                  'Description': description,
                  'Tag': tag,
                  'OperatingSystem': os,
                  'ResponsiblePerson': responsible,
                  'UserPerson': mainuser,
                  'LandbManagerPerson': manager,
                  'IPv6Ready': ipv6ready,
                  'ManagerLocked': manager_locked,
                  'SerialNumber': serialnumber}

        # result = self.client.service.deviceInsert(device)
        result = self.client.service.vmCreate(device,
                                              {'VMParent': None})

        if not result:
            raise Exception(
                f"failed to create device :: {device_name} :: {result.fault}")

    def vm_delete(self, device_name, verify_no_interfaces=True):
        """Remove a device (and all it's card/interfaces) from LanDB.

        If verify_no_interfaces is set, it will check that no interfaces are
        remaining on the device
        """
        try:
            if verify_no_interfaces:
                # device_info will also throw exception if not found
                device = self.getDeviceInfo(device_name)
                if device.Interfaces and len(device.Interfaces) > 0:
                    raise Exception(
                        "Verification failed at device_delete. There "
                        "are interfaces left on the device.")

            result = self.client.service.vmDestroy(device_name)
            # result = self.client.service.deviceRemove(device_name)

            if not result:
                raise Exception(
                    "failed to delete device "
                    f":: {device_name} :: {result.fault}")
        except exception.CernLanDBNotFound:
            pass
        except WebFault as ex:
            if "NOTFOUND#" in ex.fault.detail:
                LOG.warning(f"Device {device_name} not found in LanDB")
                return
            else:
                raise ex

    def _verify_and_return_interface(self, device, interface_name):
        device_info = self.getDeviceInfo(device)
        for i in device_info.Interfaces:
            if i.Name.upper() == interface_name.upper():
                return i

        LOG.error("Cannot find device interface: %s", interface_name)
        msg = ("%s - Can't find interface name" % str(interface_name))
        raise exception.CernInvalidHostname(msg)
        return None

    def __recover_alias(self, device, interface_name, old_alias):
        """Try to recover alias after an error"""
        LOG.info("Trying to recover old alias")
        landb_interface = self._verify_and_return_interface(
            device, interface_name)
        current_alias = landb_interface.IPAliases

        if current_alias:
            for alias in current_alias:
                self.__unset_alias(interface_name, alias)
        try:
            for alias in old_alias:
                self.__set_alias(interface_name, alias)
        except Exception as e:
            LOG.error("Cannot recover all alias: %s", e)
            raise exception.CernLanDBUpdate()

    def __set_alias(self, interface_name, alias):
        """Set alias to a device"""
        try:
            self.client.service.interfaceAddAlias(interface_name, alias)
        except Exception as e:
            LOG.error("Cannot set alias in landb: %s", e)
            raise exception.CernLanDBUpdate()

    def __unset_alias(self, interface_name, alias):
        """Unset all alias in a device"""
        try:
            self.client.service.interfaceRemoveAlias(interface_name, alias)
        except Exception as e:
            LOG.error("Cannot unset alias in landb: %s", e)
            raise exception.CernLanDBUpdate()

    def alias_update(self, device, interface_name, new_alias):
        """Update alias"""
        landb_interface = self._verify_and_return_interface(
            device, interface_name)
        old_alias = landb_interface.IPAliases

        interface_domain = interface_name.split('.', 1)[1].strip().upper()
        for alias_temp in new_alias:
            if not alias_temp.upper().endswith(interface_domain):
                LOG.error("Alias domain doesn't match interface domain: %s",
                          alias_temp)
                msg = ("%s - Alias domain doesn't match interface "
                       "domain" % str(alias_temp))
                raise exception.CernInvalidHostname(msg)

        if old_alias is None:
            old_alias = []

        old_alias_set = set([x.upper() for x in old_alias])
        new_alias_set = set([x.upper() for x in new_alias if x != ''])

        add_alias = new_alias_set - old_alias_set
        remove_alias = old_alias_set - new_alias_set

        for alias in add_alias:
            if self.device_exists(alias):
                LOG.error("Alias already exists: %s", str(alias))
                msg = ("%s - The device already exists or is not "
                     "a valid hostname" % str(alias))
                raise exception.CernInvalidHostname(msg)

        try:
            for alias in remove_alias:
                self.__unset_alias(interface_name, alias)

            for alias in add_alias:
                self.__set_alias(interface_name, alias)
        except Exception:
            self.__recover_alias(device, interface_name, old_alias)
            msg = ("%s - The device already exists or is not "
                   "a valid hostname" % str(alias))
            raise exception.CernInvalidHostname(msg)

    def alias_delete(self, device):
        device_info = self.getDeviceInfo(device)
        for i in device_info.Interfaces:
            if i.Name.upper().endswith("-IPMI.CERN.CH"):
                continue

            old_alias = i.IPAliases
            if old_alias is None:
                old_alias = []
            remove_alias = set([x.upper() for x in old_alias])

            try:
                for alias in remove_alias:
                    self.__unset_alias(i.Name.upper(), alias)
            except Exception:
                raise exception.CernLanDBUpdate()

    def ipv6ready_update(self, device, boolean):
        """Update ipv6 ready flag"""
        try:
            self.client.service.deviceUpdateIPv6Ready(device, boolean)
        except Exception as e:
            LOG.error("Cannot change IPv6-ready: %s", e)
            raise exception.CernLanDBUpdate()

    def internet_update(self, device, interface, boolean):
        """Update VM internet connectivity"""
        clusters = self.vmGetClusterMembership(device)
        try:
            if not clusters or len(clusters) == 0:
                vm_cluster = None
            elif len(clusters) == 1:
                vm_cluster = clusters[0]
            else:
                # For hosts that have multiple VMPools, we need to find
                # the right one
                iname = interface.split('.')[0].upper().strip()
                landb_device = self.client.service.getDeviceInfo(device)
                service_name = None
                if landb_device.Interfaces:
                    for i in landb_device.Interfaces:
                        if i.Name.split('.')[0].upper().strip() == iname:
                            service_name = i.ServiceName
                            break

                for p in clusters:
                    c = self.client.service.vmClusterGetInfo(p)
                    if service_name in c.Services:
                        vm_cluster = p
                        break
        except Exception as e:
            LOG.error("Can't find netcluster: %s", e)
            raise exception.CernLanDBUpdate()

        landb_interface = self._verify_and_return_interface(device, interface)
        ipv4 = landb_interface.IPAddress.__str__()
        ipv6 = landb_interface.IPv6Address.__str__()

        if boolean:
            internet = 1
        else:
            internet = 0

        vm_interface_options = {
            'IP': ipv4,
            'IPv6': ipv6,
            'InternetConnectivity': internet
        }

        try:
            self.client.service.vmMoveInterface(
                device, interface, vm_cluster, vm_interface_options)
        except Exception as e:
            LOG.error("Cannot update internet connectivity: %s", e)
            raise exception.CernLanDBUpdate()

    def vm_migrate(self, hostname, node):
        """Migrate VM to node"""
        try:
            self.client.service.vmMigrate(hostname, node)
        except Exception as e:
            LOG.error("Cannot migrate device in lanDB: %s", e)
            return False
        return True

    def device_hostname(self, address):
        """Get the hostname given an IP"""
        try:
            device = (
                self.client.service.searchDevice({'IPAddress': address}))[0]
        except Exception as e:
            LOG.error("Cannot find device with IP: %s", e)
            raise exception.CernDeviceNotFound('')
        return device

    def get_metadata(self, instance, client_xldap, client_key):
        metadata = {}
        metadata['responsible'] = self._get_metadata_projectresp(client_key,
                                                        client_xldap, instance)
        metadata['mainuser'] = self._get_metadata_mainuser(
            client_key, client_xldap, instance)
        if metadata['mainuser'] is None or metadata['responsible'] is None:
            LOG.error("Failed to find main user or responsible")
            raise exception.CernInvalidUserEgroup()
        metadata['description'] = self._get_metadata_description(instance)
        metadata['connectivity'] = self._get_metadata_connectivity(instance)
        metadata['ipv6ready'] = self._get_metadata_ipv6ready(instance)

        return metadata

    def _get_metadata_mainuser(self, client_key, client_xldap, instance):
        mainuser = None
        user_id = None
        egroup_id = None

        # start with the user id of the instance (lowest priority)
        tmp_user = client_xldap.user_exists(instance.user_id)
        if tmp_user:
            user_id = tmp_user

        # overwrite with the project main user if defined (medium)
        tmp_user = client_key.get_project_mainuser(instance.project_id)
        if tmp_user:
            user_id = client_xldap.user_exists(tmp_user)
            egroup_id = client_xldap.egroup_exists(tmp_user)

        # overwrite with instance main user metadata if defined (highest)
        if 'landb-mainuser' in instance.metadata.keys():
            tmp_user = instance.metadata['landb-mainuser']
            user_id = client_xldap.user_exists(tmp_user)
            egroup_id = client_xldap.egroup_exists(tmp_user)

        # prefer user, otherwise egroup
        if user_id:
            mainuser = {'PersonID': user_id}
        elif egroup_id:
            mainuser = {'FirstName': 'E-GROUP', 'Name': egroup_id}
        else:
            LOG.error("Failed to find main user '%s' ", mainuser)
            raise exception.CernInvalidUserEgroup()

        return mainuser

    def _get_metadata_projectresp(self, client_key, client_xldap, instance):
        respuser = None
        user_id = None
        egroup_id = None

        # start with the user id of the instance (lowest priority)
        tmp_user = client_xldap.user_exists(instance.user_id)
        if tmp_user:
            user_id = tmp_user

        # overwrite with the project responsible if defined (medium priority)
        tmp_user = client_key.get_project_responsible(instance.project_id)
        if tmp_user:
            user_id = client_xldap.user_exists(tmp_user)
            egroup_id = client_xldap.egroup_exists(tmp_user)

        # overwrite with the instance responsible metadata if defined (highest)
        if 'landb-responsible' in instance.metadata.keys():
            tmp_user = instance.metadata['landb-responsible']
            user_id = client_xldap.user_exists(tmp_user)
            egroup_id = client_xldap.egroup_exists(tmp_user)

        # prefer user, otherwise egroup
        if user_id:
            respuser = {'PersonID': user_id}
        elif egroup_id:
            respuser = {'FirstName': 'E-GROUP', 'Name': egroup_id}
        else:
            LOG.error("cern-landb: failed to find responsible '%s'",
                      respuser)
            raise exception.CernInvalidUserEgroup()

        return respuser

    def _get_metadata_description(self, instance):
        description = ""
        if 'landb-description' in instance.metadata.keys():
            description = instance.metadata['landb-description']
        return description

    def _get_metadata_connectivity(self, instance):
        if ('landb-internet-connectivity' in instance.metadata.keys() and
                instance.metadata[
                    'landb-internet-connectivity'].lower() == 'false'):
            return False
        return True

    def _get_metadata_ipv6ready(self, instance):
        if ('landb-ipv6ready' in instance.metadata.keys() and
                instance.metadata['landb-ipv6ready'].lower() == 'true'):
            return True
        return False

    def device_addcard(self, device, mac):
        """Add Card"""
        try:
            self.client.service.vmAddCard(
                device, {"HardwareAddress": mac, "CardType": "Ethernet"})
        except Exception as e:
            LOG.error("Cannot add card - %s", e)
            raise exception.CernLanDB()

    def device_removecard(self, device, mac):
        """Remove Card"""
        try:
            self.client.service.vmRemoveCard(device, mac)
        except Exception as e:
            LOG.error("Cannot remove card - %s", e)
            raise exception.CernLanDB()

    def bind_unbind_interface(self, interface_name, mac):
        """Binds an interface to a card. Empty MAC unsets the binding"""
        if not interface_name.upper().endswith(".CERN.CH"):
            interface_name += ".CERN.CH"
        try:
            self.client.service.bindUnbindInterface(interface_name, mac)
        except Exception as e:
            LOG.error("Cannot bind interface - %s", e)
            # Do not throw exceptions, since not required method mostly
            # raise exception.CernLanDB()

    def device_addinterface(self, device, ipv4, ipv6, service_name,
                            cluster_name, interface_name):
        """Add Interface"""
        if ipv4 is None and ipv6 is None:
            raise exception.CernLanDB()

        # Check if both are valid
        try:
            if ipv4:
                ipaddress.ip_address(ipv4)
            else:
                ipv4 = ''
            if ipv6:
                ipaddress.ip_address(ipv6)
            else:
                ipv6 = ''
        except ValueError as e:
            LOG.error("Cannot validate ips - %s", e)
            raise exception.CernLanDB()

        vm_interface_options = {
            'IP': ipv4,
            'IPv6': ipv6,
            'ServiceName': service_name,
            'InternetConnectivity': 'Y'
        }

        try:
            self.client.service.vmAddInterface(
                device, interface_name + '.CERN.CH',
                cluster_name, vm_interface_options)
        except Exception as e:
            LOG.error("Cannot add interface - %s", e)
            raise exception.CernLanDB()

    def device_moveinterface(self, device, interface_name):
        """Move Interface"""
        if not interface_name.upper().endswith(".CERN.CH"):
            interface_name += ".CERN.CH"
        try:
            self.client.service.interfaceMove(interface_name, device)
        except Exception as e:
            LOG.error("Cannot move interface - %s", e)
            raise exception.CernLanDB()

    def device_removeinterface(self, device, ip_address):
        """Remove Interface"""
        interface_name = None

        device_info = self.getDeviceInfo(device)
        for i in device_info.Interfaces:
            if i.IPAddress == ip_address or i.IPv6Address == ip_address:
                interface_name = i.Name
                break
        else:
            LOG.error("Cannot find device interface for IP: %s", ip_address)
            raise exception.CernLanDBUpdate()
            return

        try:
            self.client.service.vmRemoveInterface(device, interface_name)
        except Exception as e:
            LOG.error("Cannot remove interface - %s", e)
            raise exception.CernLanDB()

    def device_search(self, search):
        """Device Search"""
        devices = []
        try:
            devices = self.client.service.searchDevice({'Name': search})
        except Exception:
            LOG.error("Can't search devices")
            raise exception.CernLanDB()
        return devices

    def lb_consistency(self, lb_alias):
        alias = (lb_alias.upper()).split('--LOAD-')[0].strip()
        devices = []
        # Search for the alias itself
        devices += self.device_search(alias)
        # Search for LB aliases
        devices += self.device_search(alias + '--LOAD*')

        alias_devices = []
        for device in (devices or []):
            try:
                device_interfaces = (
                    self.client.service.getDeviceInfo(device).Interfaces)
            except Exception:
                raise exception.CernLanDB()
            for interface in (device_interfaces or []):
                for interface_alias in (interface.IPAliases or []):
                    ialias = interface_alias.upper()
                    if ((alias + '--LOAD-' in ialias) and
                            ((ialias.split('.CERN.CH')[0])[
                            -1] == '-')):
                        alias_devices.append(device.upper())
        return alias_devices

    def get_projects_set(self, set_name):
        try:
            description = self.client.service.getSetInfo(
                set_name.lower()).Description
        except Exception:
            LOG.error("Can't get the lanDB set description: %s", set_name)
            return []

        os_projects = []
        if 'openstack_project' in description.lower().replace(" ", ""):
            try:
                os_temp1 = description.lower().split(
                    'openstack_project', 1)[1].strip()
                os_temp2 = os_temp1.split('=', 1)[1].strip()
                os_temp3 = os_temp2.split(';', 1)[0].strip()
                os_temp4 = os_temp3.split(',')
                os_projects = [i.strip() for i in os_temp4]
            except Exception:
                pass

        return os_projects

    def get_addresses_set(self, set_name):
        addresses = []
        try:
            addresses = self.client.service.getSetInfo(
                set_name.lower()).Addresses
        except Exception:
            LOG.warning("Can't get the addresses from the lanDB set: %s",
                        set_name)
        return addresses

    def insert_address_set(self, device, set_name, address):
        self._verify_and_return_interface(device, address)
        try:
            self.client.service.setInsertAddress(set_name, address)
        except Exception:
            LOG.error("Can't insert address in lanDB set: %s", set_name)
            raise exception.CernSetNoPermission()

    def delete_address_set(self, device, set_name, address):
        self._verify_and_return_interface(device, address)
        try:
            self.client.service.setDeleteAddress(set_name, address)
        except Exception:
            LOG.error("Can't delete address in lanDB set: %s", set_name)

    def update_address_set(self, device, set_name, old_set_name, address):
        self._verify_and_return_interface(device, address)
        if old_set_name is not None:
            if old_set_name.upper() == set_name.upper():
                return
            addresses = self.get_addresses_set(old_set_name)
            if address.upper() in addresses:
                self.delete_address_set(device, old_set_name, address)
        self.insert_address_set(device, set_name, address)

    def isIronic(self, device):
        """Check if the device is managed by Ironic"""
        try:
            metadata = self.client.service.getDeviceBasicInfo(device.upper())
            if metadata.Tag == 'OPENSTACK IRONIC NODE':
                return True
        except Exception as e:
            LOG.error("Cannot get lanDB metadata: %s - %s", device, e)
            raise exception.CernLanDB()
        return False

    def cluster_by_service_host(self, hostname: str, service_name: str):
        """Find cluster by service & hostname.

        The function returns the LanDB cluster name associated with the
        hostname. If more than one cluster is associated with the host,
        service_name is used to filter the correct one.

        This function exists already in python-landbclient
        """

        host = hostname.split('.')[0]
        clusters = self.client.service.vmGetClusterMembership(host)

        if not clusters or len(clusters) == 0:
            return None

        if len(clusters) == 1:
            # We assume the only cluster contains the required service
            return clusters[0]

        # For hosts that have multiple VMPools, we need to find the right one
        for c_name in clusters:
            c_info = self.client.service.vmClusterGetInfo(c_name)
            if service_name in c_info.Services:
                return c_name

        return None
