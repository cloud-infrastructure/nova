#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from nova.exception import Invalid
from nova.exception import NovaException


class CernProjectTargetCell(NovaException):
    msg_fmt = "Failed to select available cell."


class CernDNS(NovaException):
    msg_fmt = "Failed to update DNS."


class CernNetwork(NovaException):
    msg_fmt = "Network inconsistency."


class CernHostnameWrong(NovaException):
    msg_fmt = "Invalid hostname."


class CernInvalidHostname(Invalid):
    msg_fmt = "Device already exists or is not a valid hostname."
    code = 404


class CernInvalidUser(Invalid):
    msg_fmt = "Invalid user."
    code = 404


class CernInvalidEgroup(Invalid):
    msg_fmt = "Invalid egroup."
    code = 404


class CernInvalidUserEgroup(Invalid):
    msg_fmt = "Invalid user or egroup."
    code = 404


class CernInvalidDevice(Invalid):
    msg_fmt = "Invalid device."
    code = 404


class CernInvalidLB4Device(Invalid):
    msg_fmt = ("There are lanDB interfaces using the same DNS LB alias "
               " that don't belong to this OpenStack project.")
    code = 403


class CernSetNoPermission(Invalid):
    msg_fmt = ("The lanDB Set doesn't exist or this OpenStack Project "
               "can't add/remove addresses in the Set.")
    code = 403


class CernDeviceNotFound(NovaException):
    msg_fmt = "Device not found."


class CernLanDB(NovaException):
    msg_fmt = "Unable to connect to LanDB"


class CernLanDBAuthentication(NovaException):
    msg_fmt = "Unable to authenticate to LanDB"


class CernLanDBUpdate(NovaException):
    msg_fmt = "Unable to update LanDB"


class CernLanDBNotFound(NovaException):
    msg_fmt = "LanDB entry not found"


class CernActiveDirectory(NovaException):
    msg_fmt = "Network Error"
