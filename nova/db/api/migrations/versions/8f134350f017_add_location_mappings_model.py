# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

"""Add location mappings model

Revision ID: 8f134350f017
Revises: b30f573d3377
Create Date: 2023-10-13 08:26:55.301494
"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8f134350f017'
down_revision = 'b30f573d3377'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('location_mappings',
    sa.Column('created_at', sa.DateTime(), nullable=True),
    sa.Column('updated_at', sa.DateTime(), nullable=True),
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('host', sa.String(length=255), nullable=False),
    sa.Column('location', sa.String(length=255), nullable=False),
    sa.Column('zone', sa.String(length=255), nullable=False),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('host', name='uniq_location_mappings0host')
    )
    with op.batch_alter_table('location_mappings', schema=None) as batch_op:
        batch_op.create_index('host_lm_idx', ['host'], unique=False)
