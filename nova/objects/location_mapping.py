#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from oslo_db import exception as db_exc

from nova.cern.landb import LanDB
from nova import context
from nova.db.api import api as api_db_api
from nova.db.api import models as api_models
from nova import exception
from nova.i18n import _
from nova.objects import base
from nova.objects import fields


def _apply_updates(context, db_mapping, updates):
    db_mapping.update(updates)
    db_mapping.save(context.session)
    return db_mapping


@base.NovaObjectRegistry.register
class LocationMapping(base.NovaTimestampObject, base.NovaObject):
    # Version 1.0: Initial version
    VERSION = '1.0'

    fields = {
        'id': fields.IntegerField(read_only=True),
        'host': fields.StringField(),
        'location': fields.StringField(),
        'zone': fields.StringField(),
        }

    @staticmethod
    def _from_db_object(context, location_mapping, db_location_mapping):
        for key in location_mapping.fields:
            db_value = db_location_mapping.get(key)
            setattr(location_mapping, key, db_value)
        location_mapping.obj_reset_changes()
        location_mapping._context = context
        return location_mapping

    @staticmethod
    @api_db_api.context_manager.reader
    def _get_by_host_from_db(context, host):
        db_mapping = context.session.query(api_models.LocationMapping)\
            .filter(api_models.LocationMapping.host == host).first()
        if not db_mapping:
            raise exception.LocationMappingNotFound(name=host)
        return db_mapping

    @base.remotable_classmethod
    def get_by_host(cls, context, host):
        db_mapping = cls._get_by_host_from_db(context, host)
        return cls._from_db_object(context, cls(), db_mapping)

    @staticmethod
    @api_db_api.context_manager.writer
    def _create_in_db(context, updates):
        db_mapping = api_models.LocationMapping()
        return _apply_updates(context, db_mapping, updates)

    @base.remotable
    def create(self):
        changes = self.obj_get_changes()
        db_mapping = self._create_in_db(self._context, changes)
        self._from_db_object(self._context, self, db_mapping)

    @staticmethod
    @api_db_api.context_manager.writer
    def _save_in_db(context, obj, updates):
        db_mapping = context.session.query(api_models.LocationMapping)\
            .filter_by(id=obj.id).first()
        if not db_mapping:
            raise exception.LocationMappingNotFound(name=obj.host)
        return _apply_updates(context, db_mapping, updates)

    @base.remotable
    def save(self):
        changes = self.obj_get_changes()
        db_mapping = self._save_in_db(self._context, self, changes)
        self._from_db_object(self._context, self, db_mapping)
        self.obj_reset_changes()

    @staticmethod
    @api_db_api.context_manager.writer
    def _destroy_in_db(context, host):
        result = context.session.query(api_models.LocationMapping)\
            .filter_by(host=host).delete()
        if not result:
            raise exception.LocationMappingNotFound(name=host)

    @base.remotable
    def destroy(self):
        self._destroy_in_db(self._context, self.host)


@base.NovaObjectRegistry.register
class LocationMappingList(base.ObjectListBase, base.NovaObject):
    # Version 1.0: Initial version
    VERSION = '1.0'

    fields = {
        'objects': fields.ListOfObjectsField('LocationMapping'),
    }

    @staticmethod
    @api_db_api.context_manager.reader
    def _get_from_db(context):
        query = context.session.query(api_models.LocationMapping)
        return query.all()

    @base.remotable_classmethod
    def get_all(cls, context):
        db_mappings = cls._get_from_db(context)
        return base.obj_make_list(context, cls(), LocationMapping,
                                  db_mappings)


def _create_location_mapping(location_mapping):
    try:
        location_mapping.create()
    except db_exc.DBDuplicateEntry:
        raise exception.LocationMappingExists(name=location_mapping.host)


def _check_and_create_node_location_mappings(ctxt, compute_nodes, status_fn):
    location_mappings = []
    landb_client = LanDB()
    for compute in compute_nodes:
        status_fn(_("Checking location mapping for compute host "
                    "'%(host)s'") % {'host': compute.host})
        try:
            LocationMapping.get_by_host(ctxt, compute.host)
        except exception.LocationMappingNotFound:
            status_fn(_("Creating host mapping for compute host "
                        "'%(host)s'") % {'host': compute.host})
            hostname = compute.host.split('.')[0]
            device_info = landb_client.getDeviceInfo(hostname)
            location = "%s-%s-%s" % (
                device_info.Location.Building,
                device_info.Location.Floor,
                device_info.Location.Room)

            location_mapping = LocationMapping(
                ctxt, host=compute.host,
                location=location,
                zone=device_info.Zone
            )
            _create_location_mapping(location_mapping)
            location_mappings.append(location_mapping)
    return location_mappings


def _check_and_create_location_mappings(ctxt, cm, status_fn):
    from nova import objects
    compute_nodes = objects.ComputeNodeList.get_all(ctxt)
    added_hm = _check_and_create_node_location_mappings(
        ctxt, compute_nodes, status_fn)
    return added_hm


def discover_location_mappings(ctxt, cell_uuid=None, status_fn=None):
    from nova import objects

    if not status_fn:
        status_fn = lambda x: None

    if cell_uuid:
        cell_mappings = [objects.CellMapping.get_by_uuid(ctxt, cell_uuid)]
    else:
        cell_mappings = objects.CellMappingList.get_all(ctxt)
        status_fn(_('Found %s cell mappings.') % len(cell_mappings))

    location_mappings = []
    for cm in cell_mappings:
        if cm.is_cell0():
            status_fn(_('Skipping cell0 since it does not contain hosts.'))
            continue
        if 'name' in cm and cm.name:
            status_fn(_("Getting computes from cell '%(name)s': "
                        "%(uuid)s") % {'name': cm.name,
                                       'uuid': cm.uuid})
        else:
            status_fn(_("Getting computes from cell: %(uuid)s") %
                      {'uuid': cm.uuid})
        with context.target_cell(ctxt, cm) as cctxt:
            added_lm = _check_and_create_location_mappings(
                cctxt, cm, status_fn)
            status_fn(_('Found %(num)s location computes in cell: %(uuid)s') %
                      {'num': len(added_lm),
                       'uuid': cm.uuid})
            location_mappings.extend(added_lm)
    return location_mappings
