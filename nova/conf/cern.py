#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from oslo_config import cfg

cern_group = cfg.OptGroup('cern',
                          title='CERN integration options',
                          help="""
These options represent the tunables to integrate Nova into
the CERN Cloud service. You can use them to enable/disable
the integration code.
""")

cern_opts = [
    cfg.StrOpt('keystone_endpoint',
        default='http://127.0.0.1/v3',
        secret=True,
        help='Keystone endpoint'),
    cfg.StrOpt('activedirectory_endpoint',
        default='http://compmgtsvc.web.cern.ch'
                '/compmgtsvc/compmgtsvc.asmx?wsdl',
        secret=True,
        help='Soap active directory endpoint'),
    cfg.StrOpt('landb_username',
        default='vmmaster', secret=True,
        help='Landb username'),
    cfg.StrOpt('landb_password',
        default='', secret=True,
        help='Landb password'),
    cfg.BoolOpt('landb_dry_run',
        default=False, secret=False,
        help='Whether to ignore actual changes to LanDB'),
    cfg.StrOpt('vm_landb_manager',
        default='ai-openstack-admin', secret=True,
        help='Landb Manager'),
    cfg.ListOpt('hide_availability_zones',
        default=[''],
        help="Hide a list of availability zones from the users"),
    cfg.StrOpt('xldap_url',
        default='ldap://xldap.cern.ch',
        secret=True,
        help='Url to connect to xldap'),
    cfg.BoolOpt('utils_attach_interface',
        default=False,
        help="Triggers the integration with landb on attachment"),
    cfg.BoolOpt('utils_attach_use_segments_instead_of_netcluster',
        default=False,
        help="When registering an interface in LanDB query for the VMPOOL"
        " using segments instead of netcluster API"),
    cfg.BoolOpt('utils_detach_interface',
        default=False,
        help="Triggers the integration with landb on detachment"),
    cfg.BoolOpt('utils_describe_availability_zones',
        default=False,
        help="Filter out availability zones to the end user"),
    cfg.BoolOpt('utils_update_instance_metadata',
        default=False,
        help="Triggers integration with landb on update metadata"),
    cfg.BoolOpt('utils_delete_instance_metadata',
        default=False,
        help="Triggers integration with landb on delete metadata"),
    cfg.BoolOpt('utils_update_landb_on_show',
        default=False,
        help="Triggers integration with landb update on show"),
    cfg.BoolOpt('utils_validate_instance_creation',
        default=False,
        help="Validates instance before creation"),
    cfg.BoolOpt('utils_validate_instance_rebuild',
        default=False,
        help="Validates instance before rebuild"),
    cfg.BoolOpt('change_display_name_ec2',
        default=False,
        help="Changes display name for EC2 instances"),
    cfg.BoolOpt('avoid_scheduler_on_rebuild',
        default=False,
        help="Avoid running the scheduler while rebuilding instances"),
    cfg.BoolOpt('landb_create_vm_and_verify_ironic_name',
        default=False,
        help="Triggers the registration of a LanDB device during create"),
    cfg.BoolOpt('landb_delete_for_local_delete',
        default=False,
        help="Triggers the deregistration of LanDB devices when not :"
             "scheduled/L0 deletes the instance"),
    cfg.BoolOpt('utils_build_instance',
        default=False,
        help="Triggers integration steps on build instance"),
    cfg.BoolOpt('utils_terminate_instance',
        default=False,
        help="Triggers integration steps before terminating an instance"),
    cfg.BoolOpt('utils_allocate_network_ironic',
        default=False,
        help="Triggers integration steps for adding network on ironic"),
    cfg.BoolOpt('utils_deallocate_network_ironic',
        default=False,
        help="Triggers integration steps for removing network on ironic"),
    cfg.BoolOpt('utils_ironic_empty_network_info',
        default=False,
        help="Ignores network information in ironic operations"),
    cfg.BoolOpt('ignore_sync_ironic',
        default=False,
        help="Ignores synchronization between power states in ironic"),
    cfg.BoolOpt('utils_update_port_request',
        default=False,
        help="Triggers integration with port request"),
    cfg.BoolOpt('utils_update_port_with_subnet',
        default=False,
        help="Triggers integration with update port with subnet"),
    cfg.BoolOpt('use_admin_on_port_creation',
        default=False,
        help="Use admin credentials while creating ports"),
    cfg.BoolOpt('utils_sort_subnets',
        default=False,
        help="Sort subnets while building VIF model"),
    cfg.BoolOpt('remove_reqs_for_live_migration',
        default=False,
        help="Remove hypervisor requirements to improve live migrations"),
    cfg.BoolOpt('scatter_gather_consoletoken',
        default=False,
        help="Fetch consoletoken by requesting it through all cells"),
    cfg.BoolOpt('use_prerequest_filters',
        default=False,
        help="Use request filters to process the request spec"),
    cfg.BoolOpt('use_request_filters',
        default=False,
        help="Use request filters to process the request spec"),
    cfg.BoolOpt('ignore_requested_destination',
        default=False,
        help="Ignore request destionation in the request spec"),
    cfg.BoolOpt('use_host_mapping_to_reduce_cell_calls',
        default=False,
        help="Use request filters to process the request spec"),
    cfg.BoolOpt('fix_ironic_deletion_race',
        default=False,
        help="Fix deletion race between nova, ironic, aims and landb"),
    cfg.BoolOpt('cern_get_all_flavors_policy',
        default=False,
        help="Policy to control who can list all flavors"),
    cfg.BoolOpt('ignore_hot_unplug_guest_busy',
                default=False,
                help="""
Ignores the exception when hot unplugging a device

On the ARM nodes we are getting this exception when removing
a volume. This is the current workaround and may change in
the future as this area of the code has been refactored in
newer releases."""),
    cfg.BoolOpt('location_aware_affinity',
        default=False,
        help="Enable affinity/anti- filtering based on location"),
    cfg.BoolOpt('enable_wait_dhcp',
        default=False,
        help="Enable wait_for_dhcp"),
    cfg.IntOpt('wait_dhcp_time',
        default=120,
        help="Seconds used during networking to get DHCP ready."),
    cfg.IntOpt('wait_dns_time',
        default=1200,
        help="Seconds used waiting for DNS until error."),
    cfg.StrOpt('dns_library',
        default='socket',
        choices=['socket', 'dnspython'],
        help="DNS library for dns resolution "),
    cfg.StrOpt('dns_domain',
        default='.cern.ch',
        help="DNS domain for checking if it's already in DNS"),
]


def register_opts(conf):
    conf.register_group(cern_group)
    conf.register_opts(cern_opts,
                       group=cern_group)


def list_opts():
    return {cern_group: cern_opts}
