# Copyright (c) 2011 OpenStack Foundation
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
"""
Offset Weigher. Return the value defined as property in the aggregate.

The default is to spread instances across all aggregates. If we need to disable
an aggregate, we can just introduce an offset (multiplier, value) in the
aggregate so the candidates will be pushed down in the order to schedule
instances.
"""

import nova.conf
from nova.scheduler import utils
from nova.scheduler import weights

CONF = nova.conf.CONF


class OffsetWeigher(weights.BaseHostWeigher):
    minval = 0

    def weight_multiplier(self, host_state):
        """Override the weight multiplier."""
        return utils.get_weight_multiplier(
            host_state, 'offset_weight_multiplier', 1.0)

    def _weigh_object(self, host_state, weight_properties):
        """Higher weights win."""
        return utils.get_weight_multiplier(
            host_state, 'offset_weight_value', 1.0)
